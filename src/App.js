/**
 * @copyright Elmelo Ltd.
 */

import React from 'react';
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import './App.css';
import color from './color.json';
import { bounce } from 'react-animations';
import SampleSet from './sample_cset'
import { StyleSheet, css } from 'aphrodite';


const styles = StyleSheet.create({
    bounce: {
        animationName: bounce,
        animationDuration: '1s'
    }
})
/**
 */
class App extends React.PureComponent
{
	/**
	 */
	constructor( props )
	{
		super( props )

		this.state = {
				bImgLoad: false,
				selectedFile: '',
				filePath: null,
				base64: null,
				img_w: 0,
				img_h: 0,
				win_w: 0,
				win_h: 0,

				// moveEnabled: false,

				pointClicked: 0,
				pointSelected: 0,
				// definedPoints: SamplePts,

				// classiferSet: SampleSet,
				cset: SampleSet,
			}

		this.divElement = null
	}	// cstr

	/**
	 */
	render() 
	{
		return (
			<div
				// style={{flex: 1,}}
				ref={ (div_elem) => {
					this.divElement = div_elem

					// this.ResizeHandler()
				} }
			>
                <div style={{display:"flex",flexDirection:"row",}}>
				{
					this.state.cset.carr.map( (item, itemIdx ) => (
							<div
								style={
									this.state.pointSelected === itemIdx
										?
										{backgroundColor: "#FFF", margin: 5,flex:2,padding: 5,fontWeight:"900",color:"#000",textAlign:"center",boxShadow: "1px 1px 1px 1px #9E9E9E"}
										:
										{backgroundColor:
											item.name === "A1" ?
												color.color.a1
												: item.name === "A2" ?
													color.color.a2
													: item.name === "A3" ?
														color.color.a3
														: item.name === "A4" ?
															color.color.a4
															:item.name === "A5" ?
																color.color.a5
																: item.name === "A6" ?
																	color.color.a6
																	: item.name === "A7" ?
																		color.color.a7
																		: item.name === "A8" ?
																			color.color.a8
																			:item.name === "A9" ?
																				color.color.a9
																				: item.name === "A10" ?
																					color.color.a10
																					: item.name === "A11" ?
																						color.color.a11
																						: item.name === "A12" ?
																							color.color.a12
																							: item.name === "A13" ?
																								color.color.a13
																								: item.name === "A14" ?
																									color.color.a14
																									: item.name === "A15" ?
																										color.color.a15
																										: null

											, color:"#fff", margin: 5, padding: 5, flex:1,cursor:"pointer" }

								}
								onClick={() => this.setState( { pointSelected: itemIdx } )}
							>
								{item.name}
							</div>
						)
					)
				}
                </div>

				<div style={{marginTop:16, marginBottom:16,display:"flex", flexDirection:"row", marginRight: 8}}>
					<input style={{flex:1}} type="file" onChange={this.ImgLoad} accept="image/*"/>

					<input style={{flex:1}} type="file" onChange={this.CSetLoad} accept="application/JSON"/>

					<>
					{
						this.state.pointClicked === this.state.cset.carr.length
					?	<button style={{flex:1,backgroundColor:"green", fontSize:18}} onClick={() => this.CSetSave()} >Save</button>

					:	<button style={{flex:1,fontSize:18}} onClick={() => this.CSetSave()} disabled>Save</button>
					}
					</>
				</div>

				<div>
                    <>
                    {
                        this.state.selectedFile ?
                            <TransformWrapper
                                defaultScale={1}
                                defaultPositionX={0}
								defaultPositionY={0}
								wheelEnabled={false}

                                options={{
										maxScale:8,
										minScale:1
									}}

                                wheel={{
										wheelEnabled: false,
										touchPadEnabled: false,
									}}

                                pan={{disabled: false}}
                                pinch={{ disabled: true }}
                                doubleClick={{ disabled: true }}
                            >
                                {({ zoomIn, zoomOut, resetTransform, scale, positionX, positionY, ...rest }) => {
									// this.setState( {scaleX: scale, scaleY: scale, posX: positionX, posY: positionY} )
									// this.SetScaleAndPos( {rat_w: scale, rat_h: scale, pos_x: positionX, pos_y: positionY} )

									if( this.state.scaleX !== scale || this.state.scaleY !== scale )
									{
										// this.setState( {scaleX: scale, scaleY: scale} )
									}

									if( this.state.posX !== positionX || this.state.posY !== positionY )
									{
										// this.setState( {posX: positionX, posY: positionY} )
									}

									return (
                                    <React.Fragment>
										{/*
										<div className="tools zoomBox">
                                            <button className="zoomIn" onClick={zoomIn}>(+)</button>
                                            <button className="zoomOut" onClick={zoomOut}>(-)</button>
											<button className="zoomOut" onClick={resetTransform}>(x)</button>
											<button className="zoomOut" onClick={() => this.setState({moveEnabled: !this.state.moveEnabled})}>Move</button>
										</div>
										*/}

                                        <TransformComponent style={{width: '98.7vw', height: '97.5vh',}}>
											<ImageCSet
												selectedFile={this.state.selectedFile}
												alt='test'
												imgW={this.state.img_w}
												imgH={this.state.img_h}
												cset={this.state.cset}
												// moveEnabled={ this.state.moveEnabled }
												pointSelected={ this.state.pointSelected }

												scale={scale}
												positionX={positionX}
												positionY={positionY}

												OnUpdate={ this.Update }
											/>
                                        </TransformComponent>
                                    </React.Fragment>
                                )} }
                            </TransformWrapper> :
                            null
                    }
					</>
				</div>
			</div>
		);
	}	// render

	/**
	 */
	componentDidMount()
	{
		// this.ResizeHandler()

		// window.addEventListener( 'resize', this.ResizeHandler )
	}

	/**
	 */
	Update = ( pt ) =>
	{
		// return {}

		let tmp_carr = [...this.state.cset.carr];

		// tmp_point[this.state.pointSelected].x = e.pageX;		// @todo
		// tmp_point[this.state.pointSelected].y = e.pageY;		// @todo

		// console.log( 'App: _onMouseDown: e.pageX: ', e.pageX )
		// console.log( 'App: _onMouseDown: e.pageY: ', e.pageY )
		// console.log( 'App: _onMouseDown: e.pageX / this.state.rat_w: ', e.pageX / this.state.rat_w )
		// console.log( 'App: _onMouseDown: e.pageY / this.state.rat_h: ', e.pageY / this.state.rat_h )

		// tmp_carr[this.state.pointSelected].pts = [
		// 		...tmp_carr[this.state.pointSelected].pts,
		// 		{x: e.pageX * this.state.rat_w, y: e.pageY * this.state.rat_h,}
		// 	]
		tmp_carr[this.state.pointSelected].pts = [pt]

		this.setState({
				// carr: [...tmp_carr],
				cset: {...this.cset, carr: [...tmp_carr]},
				pointSelected: this.state.bAutoProgress ? this.state.pointSelected + 1 : this.state.pointSelected,
				pointClicked: this.state.pointClicked + 1,
			});
	}

	// /**
	//  */
	// SetScaleAndPos = ( {rat_w, rat_h, pos_x, pos_y, } ) =>
	// {
	// 	this.setState( {rat_w, rat_h, pos_x, pos_y} )
	// }

	// /**
	//  */
	// ResizeHandler = () =>
	// {
	// 	if( !this.divElement )
	// 	{
	// 		return {}
	// 	}

	// 	console.log( "state : ", this.state );
	// 	console.log( "divElement : ", this.divElement );

	// 	const width = this.divElement.clientWidth
	// 	const height = this.divElement.clientHeight

	// 	const rat_w = this.state.img_w ? this.state.img_w / width : 0
	// 	const rat_h = this.state.img_h ? this.state.img_h / height : 0

	// 	this.setState( {win_w: width, win_h: height, rat_w, rat_h} )

	// 	return {}
	// }

	/**
	 */
	ImgLoad = event =>
	{
		this.setState( {bImgLoad: true} )

		const file = event.target.files[0];
		const reader = new FileReader();

		reader.readAsDataURL(file);

		reader.onload = ( file_obj ) =>
		{
			// console.log( 'App: ImgLoad: onload: file_obj: ', file_obj )

			const img_obj = new Image()

			img_obj.src = file_obj.target.result

			img_obj.onload = ( img_src ) => {
					// console.log( 'App: ImgLoad: onload: img_src: ', img_src )
					console.log( 'App: ImgLoad: onload: img_obj.width: ', img_obj.width )
					console.log( 'App: ImgLoad: onload: img_obj.height: ', img_obj.height )

					// const img_w = img_obj.width

					const rat_w = img_obj.width ? img_obj.width / this.state.win_w : 0
					const rat_h = img_obj.height ? img_obj.height / this.state.win_h : 0

					this.setState( {
							base64: reader.result,
							img_w: img_obj.width, img_h: img_obj.height,
							rat_w, rat_h,
							bImgLoad: false,
						} );
				}

			// this.setState( {base64: reader.result, bImgLoad: false} );
		}

		reader.onerror = ( e ) =>
		{
			console.error( 'App: ImgLoad: e: ', e )

			this.setState( {bImgLoad: false} )
		}

		this.setState( { filePath: event.target.files[0], selectedFile: URL.createObjectURL(event.target.files[0]) } );
	}

	/**
	 */
	CSetLoad = event =>
	{
		try
		{
			const reader = new FileReader()

			reader.readAsText( event.target.files[0])

			reader.onload = () =>
			{
				let data = null

				try
				{
					data = JSON.parse(reader.result);
				}
				catch(err)
				{
					alert( "Invalid file!" );

					return;
				}

				if( !data.points || !data.file )
				{
					alert( "Invalid file!" );

					return;
				}

				this.setState( { selectedFile: data.file, definedPoints: data.points } );
			}

			return {}
		}
		catch(err)
		{
			console.error( "App: CSetLoad: err : ", err );

			alert( "Invalid file!" );
		}
	}	// CSetLoad

	/**
	 */
	CSetSave = () =>
	{
		let jsonData = {
				...this.state.cset
				// carr: this.state.carr,
				// file: this.state.base64
			}

		const fileData = JSON.stringify(jsonData);
		const blob = new Blob([fileData], {type: "text/plain"});
		const url = URL.createObjectURL(blob);
		const link = document.createElement('a');

		link.download = this.state.filePath.name + '.json';
		link.href = url;

		link.click();
	}

	// /**
	//  */
	// _onMouseDown = (e) =>
	// {
	// 	console.log( 'App: _onMouseDown: e: ', e );
	// 	console.log( 'App: _onMouseDown: e: ', e.pageX );
	// 	console.log( 'App: _onMouseDown: e: ', e.pageY );
	// 	console.log( 'App: _onMouseDown: e: ', e.screenX );
	// 	console.log( 'App: _onMouseDown: e: ', e.screenY );
	// 	console.log( 'App: _onMouseDown: e: ', e.clientX );
	// 	console.log( 'App: _onMouseDown: e: ', e.clientY );

	// 	if( !this.state.selectedFile )
	// 	{
	// 		alert( "Select an image first" );

	// 		return;
	// 	}

	// 	if( !this.state.cset )
	// 	{
	// 		alert( 'Classifier Set is missing.' )

	// 		return {}
	// 	}

	// 	if( this.state.pointSelected >= this.state.cset.carr.length )
	// 	{
	// 		alert( 'Selected classifier is out of bounds.' )

	// 		return {}
	// 	}

	// 	let tmp_carr = [...this.state.cset.carr];

	// 	// tmp_point[this.state.pointSelected].x = e.pageX;		// @todo
	// 	// tmp_point[this.state.pointSelected].y = e.pageY;		// @todo

	// 	 console.log( 'App: _onMouseDown: e.pageX: ', e.pageX )
	// 	 console.log( 'App: _onMouseDown: e.pageY: ', e.pageY )
	// 	 console.log( 'App: _onMouseDown: e.pageX / this.state.rat_w: ', e.pageX / this.state.rat_w )
	// 	 console.log( 'App: _onMouseDown: e.pageY / this.state.rat_h: ', e.pageY / this.state.rat_h )

	// 	tmp_carr[this.state.pointSelected].pts = [
	// 			...tmp_carr[this.state.pointSelected].pts,
	// 			{x: e.pageX * this.state.rat_w, y: e.pageY * this.state.rat_h,}
	// 		]

	// 	this.setState({
	// 			// carr: [...tmp_carr],
	// 			cset: {...this.cset, carr: [...tmp_carr]},
	// 			pointSelected: this.state.bAutoProgress ? this.state.pointSelected + 1 : this.state.pointSelected,
	// 			pointClicked: this.state.pointClicked + 1,
	// 		});
	// }
}	// App

/**
 */
class ImageCSet extends React.PureComponent
{
	/**
	 */
	constructor( props )
	{
		super( props )

		this.state = {
				//
			}
	}

	/**
	 */
	render()
	{
		// console.log( 'ImageCSet: render: this.props.selectedFile: ', this.props.selectedFile )

		return (
			<div
				onMouseDown={ /* this.props.moveEnabled ? null :  */this._onMouseDown }
				// style={rootStyle}
				// style={{flex: 1,}}
			>

				<img
					src={this.props.selectedFile}
					alt='test'
					// style={rootStyle}
					// style={{flex: 1,}}
				/>

				<>
				{
					this.props.cset.carr.length !== 0
				?	this.props.cset.carr.map((item, itemIdx) =>
						<CPts {...item} key={itemIdx}
							rat_w={this.props.scale}
							rat_h={this.props.scale}
							pos_x={this.props.positionX}
							pos_y={this.props.positionY}
						/>
						)

				:	null
				}
				</>
			</div>
			)
	}	// render

	/**
	 */
	_onMouseDown = (e) =>
	{
		// console.log( 'App: _onMouseDown: e: ', e );
		// console.log( 'ImageCSet: _onMouseDown: e.pageX: ', e.pageX );
		// console.log( 'ImageCSet: _onMouseDown: e.pageY: ', e.pageY );
		// console.log( 'App: _onMouseDown: e: ', e.screenX );
		// console.log( 'App: _onMouseDown: e: ', e.screenY );
		// console.log( 'App: _onMouseDown: e: ', e.clientX );
		// console.log( 'App: _onMouseDown: e: ', e.clientY );

		// console.log( 'ImageCSet: _onMouseDown: this.props.positionX: ', this.props.positionX );
		// console.log( 'ImageCSet: _onMouseDown: this.props.positionY: ', this.props.positionY );
		// console.log( 'ImageCSet: _onMouseDown: this.props.scale: ', this.props.scale );

		if( !this.props.selectedFile )
		{
			alert( "Select an image first" );

			return;
		}

		if( !this.props.cset )
		{
			alert( 'Classifier Set is missing.' )

			return {}
		}

		if( this.props.pointSelected >= this.props.cset.carr.length )
		{
			alert( 'Selected classifier is out of bounds.' )

			return {}
		}

		// if( this.props.imgW < e.pageX || this.props.imgH < e.pageY )
		// {
		// 	alert( 'Selected point is out of bounds.' )

		// 	return {}
		// }

		const pt_tmp = {
				x: (e.pageX*this.props.scale - this.props.positionX) / this.props.scale,
				y: (e.pageY*this.props.scale - this.props.positionY) / this.props.scale,
				// x: (e.pageX - this.props.positionX) / this.props.scale,
				// y: (e.pageY - this.props.positionY) / this.props.scale,
			}

		console.log( 'ImageCSet: _onMouseDown: pt_tmp: ', pt_tmp );

		this.props.OnUpdate( pt_tmp )

		return {}

		// let tmp_carr = [...this.state.cset.carr];

		// tmp_point[this.state.pointSelected].x = e.pageX;		// @todo
		// tmp_point[this.state.pointSelected].y = e.pageY;		// @todo

		//  console.log( 'App: _onMouseDown: e.pageX: ', e.pageX )
		//  console.log( 'App: _onMouseDown: e.pageY: ', e.pageY )
		//  console.log( 'App: _onMouseDown: e.pageX / this.state.rat_w: ', e.pageX / this.state.rat_w )
		//  console.log( 'App: _onMouseDown: e.pageY / this.state.rat_h: ', e.pageY / this.state.rat_h )

		// tmp_carr[this.state.pointSelected].pts = [
		// 		...tmp_carr[this.state.pointSelected].pts,
		// 		{x: e.pageX * this.state.rat_w, y: e.pageY * this.state.rat_h,}
		// 	]

		// this.setState({
		// 		// carr: [...tmp_carr],
		// 		cset: {...this.props.cset, carr: [...tmp_carr]},
		// 		pointSelected: this.state.bAutoProgress ? this.state.pointSelected + 1 : this.state.pointSelected,
		// 		pointClicked: this.state.pointClicked + 1,
		// 	});
	}
}	// class Image

/**
 */
class CPts extends React.PureComponent
{
	render()
	{
		// console.log( 'CPts: render: this.props: ', this.props )

		return (
			<>
			{
				this.props.pts.map( (pt, i) => (
					<CPoint key={i} name={this.props.name}
						{...pt}
						rat_w={this.props.rat_w} rat_h={this.props.rat_h}
						pos_x={this.props.pos_x} pos_y={this.props.pos_y}
					/>
				) )
			}
			</>
			)
	}
}

/**
 */
class CPoint extends React.PureComponent
{
	/**
	 */
	constructor( props )
	{
		super( props );

		this.state = {
				// x: this.props.x,
				// y: this.props.y
			}
	}

	/**
	 */
	render()
	{
		// console.log( 'CPoint: render: this.props: ', this.props )

		// x: (e.pageX*this.props.scale - this.props.positionX) / this.props.scale,

		// const tmp_left = (this.props.x - this.props.pos_x) /this.props.rat_w
		// const tmp_top = (this.props.y - this.props.pos_y) /this.props.rat_h
		const tmp_left = (this.props.x*this.props.rat_w + this.props.pos_x) / this.props.rat_w
		const tmp_top = (this.props.y*this.props.rat_h + this.props.pos_y) / this.props.rat_h

		// console.log( 'CPoint: render: tmp_left: ', tmp_left )
		// console.log( 'CPoint: render: tmp_top: ', tmp_top )

		return(
			<div>
				<div style={ {
						left: (tmp_left) / this.props.rat_w,// (this.props.x+this.props.pos_x)/this.props.rat_w,
						// top: (tmp_top - 160) / this.props.rat_w, // (this.props.y+this.props.pos_y)/this.props.rat_h-160,
						top: (tmp_top - 100) / this.props.rat_w,
						position: 'absolute',
						// width: '20px', height: '20px',
						padding: '10px',
						backgroundColor: this.props.name === "A1" ?
                            color.color.a1
                            : this.props.name === "A2" ?
                            color.color.a2
                            : this.props.name === "A3" ?
                                color.color.a3
                                : this.props.name === "A4" ?
                                    color.color.a4
                                    :this.props.name === "A5" ?
                                        color.color.a5
                                        : this.props.name === "A6" ?
                                            color.color.a6
                                            : this.props.name === "A7" ?
                                                color.color.a7
                                                : this.props.name === "A8" ?
                                                    color.color.a8
                                                    :this.props.name === "A9" ?
                                                        color.color.a9
                                                        : this.props.name === "A10" ?
                                                            color.color.a10
                                                            : this.props.name === "A11" ?
                                                                color.color.a11
                                                                : this.props.name === "A12" ?
                                                                    color.color.a12
                                                                    : this.props.name === "A13" ?
                                                                        color.color.a13
                                                                        : this.props.name === "A14" ?
                                                                            color.color.a14
                                                                            : this.props.name === "A15" ?
                                                                                color.color.a15
                                                                                : null
					} }
				>
					{this.props.name}	
				</div>
			</div>
		);
	}
}	// ClassifierPoint

/**
 */
const rootStyle = 
{
	width: '98.7vw',
	height: '97.5vh',
	backgroundColor: 'white'
}
// var pointStyle = 
// {
// 	width: "5px",
// 	height: "5px",
// 	backgroundColor: "black"
// }

export default App;
