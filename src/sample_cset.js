/**
 *  @copyright Elmelo Ltd.
 */

const cset = {
        name: '',
        desc: '',
        comments: [],   // {}
        dt_create: 0,
        dt_mod: 0,
        author_create: '',
        author_mod: '',

        carr: [
                {
                    "name": "A1",
                    id: 'a1',
                    pts: [],    // {x, y, desc, comments: [{user_id: '', msg: ''}, {...}, ...]}
                    desc: '',
                    comments: [],
                },
                {
                    "name": "A2",
                    id: 'a2',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A3",
                    id: 'a3',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A4",
                    id: 'a4',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A5",
                    id: 'a5',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A6",
                    id: 'a6',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A7",
                    id: 'a7',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A8",
                    id: 'a8',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A9",
                    id: 'a9',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A10",
                    id: 'a10',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A11",
                    id: 'a11',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A12",
                    id: 'a12',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A13",
                    id: 'a13',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A14",
                    id: 'a14',
                    pts: [],    // {x, y}
                },
                {
                    "name": "A15",
                    id: 'a15',
                    pts: [],    // {x, y}
                },
            ],
    }

//
export default cset

/**
 * comment = {
 *      //
 *  }
 */
